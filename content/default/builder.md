---
date: "2019-06-17T09:54:00+08:00"
title: "builder"
weight: 10
toc: false
draft: false
menu: "sidebar"
goimport: "xorm.io/builder git https://gitea.com/xorm/builder"
gosource: "xorm.io/builder https://gitea.com/xorm/builder https://gitea.com/xorm/builder/tree/master{/dir} https://gitea.com/xorm/builder/blob/master{/dir}/{file}#L{line}"
---

# Builder - Lightweight and fast SQL builder for Go language

This is the URL of the import path for [builder](http://gitea.com/xorm/builder).
